package stereotype;

import java.util.Set;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component  // declare it as a java bean
@Scope("singleton")
public class Person {
	@Value("Bob")
	private String name;
	
	@Autowired
	// invoke auto injection
	private Address address;
	
	@Resource  // declare it as resource 
	// indicate injection from external file (javaConfig)
	private Set<String> emails;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<String> getEmails() {
		return emails;
	}

	public void setEmails(Set<String> emails) {
		this.emails = emails;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", address=" + address + ", emails=" + emails + "]";
	}

}
