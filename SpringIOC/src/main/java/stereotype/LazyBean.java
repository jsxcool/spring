package stereotype;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy  // when create a object, just give a empty object
// until you need one field, it assigns (initialization) 
public class LazyBean {
	private static int count;
	
	int val;
	public LazyBean() {
		count++;
		val = 8;
		System.out.println("a new lazy bean generated!"+ val);
	}
	
	public static int getCount() {
		return count;
	}
}
