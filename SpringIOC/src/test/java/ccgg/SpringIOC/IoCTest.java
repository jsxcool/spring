package ccgg.SpringIOC;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import mybeans.Person;
import mybeans.User;
import mybeans.Wrapper;

// package definition and config files are put into Spring container
// So we can use Spring container to do our application
public class IoCTest {

	public static void main(String[] args) {
		
		ApplicationContext actx = new FileSystemXmlApplicationContext("/resource/iocconfig.xml");
	
		User user1 = (User) actx.getBean("user");
		System.out.println(user1);
		user1.setAge(18);
		System.out.println(user1);
		
		Person person = (Person) actx.getBean("person");
		System.out.println(person);
	
		Wrapper wp = (Wrapper) actx.getBean("wp");
		System.out.println(wp);
		// setter injection (by reference)
		System.out.println(wp.getUser()==user1);
		Wrapper wp1 = (Wrapper) actx.getBean("wp1");
		System.out.println(wp1);
		Wrapper wp2 = (Wrapper) actx.getBean("wp2");
		System.out.println(wp2);
		
		
	}

}
