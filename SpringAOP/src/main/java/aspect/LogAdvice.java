package aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LogAdvice {
	
	@Before("execution(* beans.TimeBook.doAudit(..))")
	public void logBefore(JoinPoint jp) {
		String name = (String) jp.getArgs()[0];
		System.out.print(name + " starting...\n");
	}
	
	@After("execution(* beans.TimeBook.doBreak(..))")
	public void logAfter(JoinPoint jp) {
		String name = (String) jp.getArgs()[0];
		System.out.print(name + " ending...\n");
	}
	
	@AfterThrowing(pointcut="execution(* beans.TimeBook.doCheck(..))", throwing = "error")
	public void logAfter(JoinPoint jp, Throwable error) {
		String name = (String) jp.getArgs()[0];
		System.out.print(name + " has errors...\n");
		System.out.print("Error: " + error);
	}
	
	public void delete() {
		System.out.println("deleting...");
	}
	
	@Around("execution(* beans.TimeBook.doAudit(..))")
	public Object logAround(ProceedingJoinPoint pjp) throws Throwable {
		String name = (String) pjp.getArgs()[0];
		System.out.println(name + " around before...");
		Object obj = pjp.proceed();
		System.out.println(name + " around after...");
		return obj;
	}
	
}
