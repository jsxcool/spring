package ccgg.SpringAOP;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.TimeBookInterface;

public class TestAOP {

	public static void main(String[] args) {
		try(ClassPathXmlApplicationContext actx = new ClassPathXmlApplicationContext("aopconfig.xml")){
			TimeBookInterface tb = (TimeBookInterface) actx.getBean("logProxy");
			tb.doAudit("Bob");
		}
		catch(Exception e) {}
	}

}
